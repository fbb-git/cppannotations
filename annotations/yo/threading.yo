includefile(threading/intro)

lsect(THREADS)(Multi Threading)
includefile(threading/threading)

    subsect(The namespace std::this_thread)
    includefile(threading/thisthread)

    lsubsect(THREAD)(The class std::thread)
    includefile(threading/thread)

        subsubsect(Static data and threads: thread_local)
        includefile(threading/threadlocal)

        subsubsect(Exceptions and join())
        includefile(threading/joining)

    lsubsect(JTHREAD)(The class std::jthread)
    includefile(threading/jthread)

        subsubsect(std::stop_callback)
        includefile(threading/stopcallback)

lsect(MUTEX)(Synchronization (mutexes))
includefile(threading/mutex)

    subsect(Initialization in multi-threaded programs)
    includefile(threading/initialization)

    subsect(Shared mutexes)
    includefile(threading/sharedmutex)

lsect(LOCKS)(Locks and lock handling)
includefile(threading/locks)

    subsect(Name-independent declarations)
    includefile(threading/nameindep)

    lsubsect(DEADLOCKS)(Deadlocks)
    includefile(threading/deadlocks)

    subsect(Shared locks)
    includefile(threading/sharedlock)

    subsect(Scoped locks)
    includefile(threading/scopedlock)

sect(Event handling (condition variables))
includefile(threading/events)

    lsubsect(CONDVAR1)(The class std::condition_variable)
    includefile(threading/conditionvar)

    lsubsect(CONDVAR2)(The class std::condition_variable_any)
    includefile(threading/conditionany)

    lsubsect(CONDEX)(An example using condition variables)
    includefile(threading/conditionex)

lsect(ATOMIC)(Atomic actions: mutexes not required)
includefile(threading/atomic)

sect(An example: threaded quicksort)
includefile(threading/quicksort)

sect(Shared States)
includefile(threading/shared)

lsect(FUTURE)(Asynchronous return objects: std::future)
includefile(threading/future)

    subsect(The std::future_error exception and the std::future_errc enum)
    includefile(threading/futureerrc)

lsect(SHAREDFUTURE)
    (Shared asynchronous return objects: std::shared_future)
includefile(threading/sharedfuture)

lsect(ASYNC)(Starting a new thread: std::async)
includefile(threading/async)

lsect(PACKAGE)(Preparing a task for execution: std::packaged_task)
includefile(threading/packagedtask)

lsect(PROMISE)(The class `std::promise')
includefile(threading/promise)

lsect(MULTICOMP)(An example: multi-threaded compilations)
includefile(threading/compilations)

lsect(TRANSMEM)(Transactional Memory)
includefile(threading/transactionalmemory)

lsect(OSYNC)(Synchronizing output to streams)
includefile(threading/osyncstream)

    subsect(The `std::syncbuf' streambuf)
    includefile(threading/syncbuf)
    
    subsect(Multi-threaded compilations using `osyncstream')
    includefile(threading/osynccompile)
