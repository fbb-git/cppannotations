Points in time are specified by objects of the class
    hi(time_point) tt(std::chrono::time_point).

Before using the class tt(time_point) the tthi(chrono) header file must be
included.

Like tt(duration) the class tt(time_point) requires two template arguments: A
clock type and a duration type. The standard clock types define their own
tt(time_point) types using tt(nanoseconds) as their duration type 
(tt(time_point) by default uses tt(nanoseconds)).  The
following time point type definitions are therefore identical:
        verb(    time_point<standard_clock, nanoseconds>
    time_point<standard_clock>
    standard_clock::time_point )

bf(Constructors):
    itemization(
    itt(time_point()):nl()
       the default constructor is initialized to the beginning of the clock's
        emi(epoch). For tt(system_clock) it is January, 1, 1970, 00:00h, but
        note that tt(file_clock::time_point) has a different starting point
        (see the example at the end of this section);
    )
    Copy- and move-constructors as well as copy- and move-assignment operators
        (cf. chapter ref(MEMORY)) are available

bf(Operators):
    itemization(
    itt(time_point &operator+=(duration const &amount))
       adds the amount of time represented by tt(amount) to the current
        tt(time_point) object;

    itt(time_point &operator-=(duration const &amount)) subtracts 
       the amount of time represented by tt(amount) from the
        current tt(time_point) object;

    itt(Type operator<<(std::ostream &, time_point const &)) inserts a textual
        representation of the tt(time_point's) UTC time into the tt(ostream).
    )

The compound operators are also available as binary arithmetic operators using
a tt(time_point const &) and a tt(duration const &) operand (in any
order). Example:
       verb(    system_clock::now() + seconds{ 5 };)

bf(Members):
    itemization(
    itht(time_since_epoch)(duration time_since_epoch() const):nl()
       tt(duration) is the duration type used by the time point object for
        which this member is called. To convert the returned tt(duration) to a
        tt(size_t) value use tt(time_since_epoch().count());

    itt(time_point min() const):
       a static member returning the time point's tt(duration::min)
        value. Example:
       verb(    cout << 
        time_point<system_clock>::min().time_since_epoch().count() << '\n';
        // shows -9223372036854775808)

    itt(time_point max() const): 
       a static member returning the time point's tt(duration::max)
        value.
    )

bf(Conversions:)

All predefined clocks use nanoseconds for their duration types. Converting to
different tt(duration) types is covered in the previous section
(ref(DURATION)). Alternatively, the members tt(to_time_t) of the
tt(system_clock) and the tt(high_resolution_clock) can be used to convert
their tt(time_point) values to tt(time_t). Such tt(time_t) values are commonly
used to convert time to text, e.g., using the em(manipulator) tt(put_time)
(cf. section ref(IOFORMAT)).  The tt(put_time) manipulator must be provided
with the address of a tt(std::tm) object, which can be obtained from a
tt(std::time_t) value. See figure ref(TIMEIMG) for a visual representation of
the involved elements.
    figure(threading/time)(Time according to bf(C++))(TIMEIMG)

Use the following blueprint to insert a tt(system_clock::time point)
value into a tt(std::ostream) (cf. section ref(PUTTIME)):
    verbinsert(-s4 //demo examples/puttime.cc)

To convert a tt(time_point) to another tt(std::chrono) clock's tt(time_point)
the template
    hi(clock_cast)
 tt(DestClock::time_point std::chrono::clock_cast<DestClock>(TimePoint tp))
 can be used. The argument tt(tp) is an available tt(time_point), and
tt(DestClock) specifies the clock type of the returned tt(time_point). E.g.,
the following tt(cout) statement displays four identical times:
 verb(    auto fNow = file_clock::now();
    auto sNow = file_clock::to_sys(fnow);

    cout << fNow << '\n' <<
        sNow << '\n' <<
        clock_cast<system_clock>(fNow) << '\n' <<
        clock_cast<file_clock>(sNow) << '\n'; )

So, where is the starting point of the tt(file_clock) clock? nl()
 Since tt(time_point's) default cconstructor is initialized to its clock's
starting point it can be obtained this way:
 verb(    cout << file_clock::time_point{}<< '\n';
    // shows:  2174-01-01 00:00:00.000000000 )
         
