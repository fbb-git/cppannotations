
#include <iostream>
#include <chrono>
#include <filesystem>

using namespace std;
using namespace chrono;
using namespace filesystem;

namespace fs = filesystem;

auto local_time_point(time_point<system_clock> const &tp, char const *fmt)
{
    time_t secs = system_clock::to_time_t( tp );
    return put_time(localtime(&secs), fmt);
}

int main()
{
//    system_clock::duration ds{ 24h };
//    file_clock::duration df{ 1h };

    file_clock::time_point p1 = fs::last_write_time("clock.cc");
    file_clock::time_point p2 = fs::last_write_time("clock.cc");

    cout << (p1 == p2) << '\n';
return 0;

    system_clock::from_time_t(
        system_clock::to_time_t(
            system_clock::from_time_t(
                12345
            )
        )
    );

        std::time_t tm {time(0)};
        std::cout << std::put_time(std::localtime(&tm), "%c") << '\n';

    time_t secs = system_clock::to_time_t( system_clock::now() );
    cout << put_time(std::localtime(&secs), "%c\n");

    cout << local_time_point(system_clock{}.now(), "%c") << '\n';

//    file_clock::to_time_t(file_clock::from_time_t(12345));
    high_resolution_clock::to_time_t(high_resolution_clock::from_time_t(12345));
//    steady_clock::to_time_t(steady_clock::from_time_t(12345));

//    unsigned uval = system_clock::period::num;

//    file_clock::period rf;


//    system_clock::rep vs;
//    file_clock::rep vf;

//    system_clock::time_point st = system_clock{}.now();

    file_clock::time_point ft;
}
