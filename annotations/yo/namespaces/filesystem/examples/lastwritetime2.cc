#include <iostream>
#include <filesystem>
#include <chrono>

using namespace std;
using namespace filesystem;
using namespace chrono;

//demo
int main()
{
    time_t seconds = system_clock::to_time_t(
                    file_clock::to_sys(last_write_time("lastwritetime.cc"))
                );

    cout << "lastwritetime.cc's last (UTC) write time: " <<
             put_time(gmtime(&seconds), "%c") << '\n';
}
//=
