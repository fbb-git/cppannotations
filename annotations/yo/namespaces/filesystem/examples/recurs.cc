#include <iostream>
#include <filesystem>

using namespace std;
using namespace filesystem;

//demo
int main()
{
    recursive_directory_iterator base{ "/var/log",
                                  directory_options::skip_permission_denied };

    for (auto entry = base, endIt = end(base); entry != endIt; ++entry)
    {
        cout << entry.depth() << ": " << *entry << '\n';
        if (entry.depth() == 1)
            entry.disable_recursion_pending();
    }
}
//=
