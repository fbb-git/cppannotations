The tt(std::path) free functions tt(status) and tt(symlink_status)
(cf. section ref(FREEFS)) return the tt(file_status) of file system entries.

The following functions provide specific information about the status of
file system entries:

COMMENT(
    The tt(filesystem) functions tt(status) and tt(symlink_status) 
retrieve or change statuses of file system entries. These
functions may be called with a final (optional) tt(error_code)
argument which is assigned an appropriate error code if they cannot perform
their tasks. If the argument is omitted the members throw exceptions if they
cannot perform their tasks
END)

    itemization(
    itht(status_known)(bool status_known(file_status const &status))
       returns tt(true) if tt(status) refers to a determined status
        (tt(status) itself may indicate that the entity referred to by
        tt(status) does not exist). One way of receiving tt(false) is by
        passing it a default status object: tt(status_known(file_status{}));

    itt(bool is_WHATEVER(file_status status)), 
       replacing tt(WHATEVER) by the requested file type, returns tt(true) if
        tt(status) refers to an entry of the specified type.
    )

These following tt(is_WHATEVER) functions are available:
    centertbl(ll)(\
    tline()()\
    tr(xcell(2)(is_WHATEVER(file_status) ))\
    tr(tlc()(Function)tlc()(Meaning))\
    tline()()\
    rowtwo(is_block_file)('entry' is a block device)
    rowtwo(is_character_file)('entry' is a character device)
    rowtwo(is_directory)('entry' is a directory)
    rowtwo(is_empty)    ('entry' is an empty file or directory)
    rowtwo(is_fifo)     ('entry' is a named pipe)
    rowtwo(is_other)    ('entry' is not a directory,nl()
                          regular file or symlink)
    rowtwo(is_regular_file)('entry' is a regular file)
    rowtwo(is_socket)   ('entry' is a named socket)
    rowtwo(is_symlink)  ('entry' is a symbolic link)
    tline()()\
    )

    Here is a small program showing how file statuses can be obtained and
displayed (cf. sections ref(PATH) and (for the em(map)) section ref(MAP)):
    verbinsert(-s4 //demo examples/statusknown.cc)


