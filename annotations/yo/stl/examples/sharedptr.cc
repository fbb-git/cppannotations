#include <memory>
#include <iostream>
using namespace std;

using si = shared_ptr<int>;
int main()
{
    si ip(new int(12));

    cout << ip.unique() << '\n';

    si ip2(ip);

    cout << ip.unique() << '\n';

    si ip3 = ip;
    si ip4;
    cout << *ip << ip.use_count() << '\n';
}
