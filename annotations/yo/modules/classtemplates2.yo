In section ref(MODTEMP) the a class template tt(SortMap) was defined in a
module interface unit. Like class templates in header files class templates in
a module interface files may grow large because the file not only contains the
class template's interface but also the implementations of its
members. Consequently a full recompilation is required when only a single
member function is modified. Recompilations cannot completely be avoided, but
by using module partitions complete recompilation can often be avoided.

Some maintenance complications, however, remain: once a class template's
member is modified the modification is not visible in members that use that
member. But this dependency issue always plays a role when using templates:
once a template has been modified all code using the template needs to be
recompiled.

In the current section class templates are implemented using module
partitions, each containing a member of the class template. The previously
developed tt(SortMap) module consists of three components: the class interface
and two tt(sort) members functions. Now when using partitions each
component will be defined in its own partition, and the module itself contains
tt(export import) statements for each partition which must be accessible by
code using the module. Consequently the tt(modsortmap.cc) module interface
unit is remarkably simple:
    verbinsert(-as4 examples/classtemplate2/sortmap/modsortmap.cc)
    Note that even the class interface is defined in a partition: it cannot be
defined in the module interface unit itself, since partitions em(can) depend
on other partitions, but not on the module itself as the partitions must (as
they are imported by the module) be available before the module itself can be
compiled. Partitions, however, em(can) depend on other partitions, so a
partition implementing a tt(sort) function can import a partition providing
the class's interface.

As the tt(:Interface) partition merely specifies what's offered by the
tt(class SortMap) it doesn't depend on the other partitions. So it merely
declares but does not contain the implementations of the tt(sort) members, and
thus it does not itself have to import the tt(alorithm) module-compiled
header:
    verbinsert(-as4 examples/classtemplate2/sortmap/interface/interface.cc)

On the other hand, the implementations of the two tt(sort) members em(do)
depend on the interface, since they implement their declarations. They
also depend on the facilities provided by the tt(algorithm) module-compiled
header. Both are therefore imported. Here's the implementation of the first
tt(sort) member in the tt(:Sort1) partition:
    verbinsert(-as4 examples/classtemplate2/sortmap/sort1/sort1.cc)

    The second tt(sort) member is implemented analogously:
    verbinsert(-as4 examples/classtemplate2/sortmap/sort2/sort2.cc)

The tt(main) function can remain as provided in section ref(MODTEMP). When
constructing the program the partition dependencies must be taken into
account:
    itemization(
    it() first the tt(:Interface) partition is compiled;
    it() then, in any order, the tt(:Sort1) and tt(:Sort2) partitions are
        compiled; 
    it() finaly tt(main.cc) is compiled and the compiled object files are
        linked resulting in the final program.
    )


    

