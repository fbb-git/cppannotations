import Adder;

import <iostream>;
import <string>;

using namespace std;

int main()
{
    cout << add(1, 2) << '\n' <<
            add(1.1, 2.2) << '\n' <<
            add("hello "s, "world"s) << '\n';
}
