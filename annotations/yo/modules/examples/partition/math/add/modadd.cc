export module Math:Add;

import :Utility;

class Add
{
    Utility &d_utility;

    public:
        Add(Utility &utility);
        size_t sum(size_t lhs, size_t rhs);
};

