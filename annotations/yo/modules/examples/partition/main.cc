import Math;
import <iostream>;

using namespace std;

int main()
{
    Math math;

    cout << "Initial number of additions: " << math.count() << "\n"
            "Enter two pos. values to add: ";

    size_t lhs;
    size_t rhs;
    cin >> lhs >> rhs;
    cout << "their sum is " << math.add(lhs, rhs) << "\n"
            "total number of performed additions: " << math.count() << '\n';
}
