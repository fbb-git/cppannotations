module Basic;

import Second;
import <iostream>;
import <string>;

using namespace std;

void Basic::hello() const
{
    cout << Second::hello() << '\n';
}
