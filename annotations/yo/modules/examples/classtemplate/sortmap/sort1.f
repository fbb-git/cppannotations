template <typename Key, typename Value>
SortMap<Key, Value>::Vect const &SortMap<Key, Value>::sort()
{
    d_sortVect.clear();

    for (auto const &el: *this)
        d_sortVect.push_back(&el);

    std::sort(d_sortVect.begin(), d_sortVect.end(), 
        [&](auto const *lhs, auto const *rhs)
        {
            return lhs->first < rhs->first;
        }
    );

    return d_sortVect;
}
