export module SortMap;

export import <unordered_map>;
export import <vector>;
import <algorithm>;

export
{
    template <typename Key, typename Value>
    class SortMap: public std::unordered_map<Key, Value>
    {
        using UMap = std::unordered_map<Key, Value>;
        using ValueType = typename UMap::value_type;
        using Vect = std::vector<ValueType const *>;

        private:
            Vect d_sortVect;

        public:
            Vect const &sort();             // sort the keys        // 1.f

            template <typename Functor>     // use a functor        // 2.f
            Vect const &sort(Functor const &functor);
    };
}

#include "sortmap.f"









