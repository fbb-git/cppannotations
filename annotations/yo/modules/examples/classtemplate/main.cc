import SortMap;

import <iostream>;
import <string>;

using namespace std;

int main(int argc, char **argv)
{
    SortMap<string, size_t> sortMap;

    for (; argc--; )                                    // fill sortMap
        sortMap.emplace(argv[argc], argc);

    for (auto const *ptr: sortMap.sort())               // sort by key
        cout << ptr->first << ' ' << ptr->second << "; ";
    cout.put('\n');

    for (auto const *ptr: sortMap.sort(                 // sort by value
                                [&](auto const &lhs, auto const &rhs)
                                {
                                    return lhs->second < rhs->second;
                                }
                            )
    )
        cout << ptr->first << ' ' << ptr->second << "; ";
    cout.put('\n');
}

    
