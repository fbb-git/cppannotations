export module Square;                           // source: modsquare.cc

export
{
    double square(double value);

    class Square
    {
        double d_amount;

        public:
            Square(double amount = 0);          // initialize
            void amount(double value);          // change d_amount
            double amount() const;              // return d_amount

            double lastSquared() const;         // returns g_squared
            double square() const;              // returns sqr(d_amount)
    };
}

extern double g_squared;
