import Square;
import <iostream>;

int main(int argc, char **argv)
{
    std::cout << "the square of " << argc << " is " << square(argc) << '\n';
    Square obj{12};
    std::cout << "the square of 12 is " << obj.square() << "\n"
                "the last computed square is " << obj.lastSquared() << '\n';

}
