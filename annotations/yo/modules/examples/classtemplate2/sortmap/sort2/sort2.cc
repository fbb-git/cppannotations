export module SortMap:Sort2;

import :Interface;
import <algorithm>;

export
{
    template <typename Key, typename Value>
    template <typename Functor>
    SortMap<Key, Value>::Vect const &SortMap<Key, Value>::sort(
                                            Functor const &functor)
    {
        d_sortVect.clear();
    
        for (auto const &el: *this)
            d_sortVect.push_back(&el);
    
        std::sort(d_sortVect.begin(), d_sortVect.end(), functor);
    
        return d_sortVect;
    }
}
