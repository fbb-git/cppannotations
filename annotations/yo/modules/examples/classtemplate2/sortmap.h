#include <iostream>

#include <unordered_map>
#include <vector>
#include <algorithm>

template <typename Key, typename Value>
struct SortMap: public std::unordered_map<Key, Value>
{
    using UMap = std::unordered_map<Key, Value>;
    using ValueType = typename UMap::value_type;

    using Vect = std::vector<ValueType const *>;

    private:
        Vect d_sortVect;

    public:
        Vect const &sort();             // sort the keys

        template <typename Functor>     // use a functor
        Vect const &sort(Functor const &functor);
};


template <typename Key, typename Value>
SortMap<Key, Value>::Vect const &SortMap<Key, Value>::sort()
{
    d_sortVect.clear();

    for (auto const &el: *this)
        d_sortVect.push_back(&el);

    std::sort(d_sortVect.begin(), d_sortVect.end(), 
        [&](auto const *lhs, auto const *rhs)
        {
            return lhs->first < rhs->first;
        }
    );

    return d_sortVect;
}

