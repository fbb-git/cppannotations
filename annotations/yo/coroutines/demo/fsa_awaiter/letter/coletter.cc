#include "letter.ih"

Letter coLetter()
{
    char ch;
    while (cin.get(ch))
    {
        if (isalpha(ch))
            cout << "at `" << ch << "' remain in letter\n";
        else if (isdigit(ch))
        {
            cout << "at `" << ch << "' from letter to digit\n";
            co_await Awaiter{ g_digit.handle() };
        }
        else
        {
            cout << "at char #" << static_cast<int>(ch) <<
                    ": from letter to start\n";
            co_await Awaiter{ g_start.handle() };
        }
    }
    co_await Awaiter{ g_done.handle() };
}
