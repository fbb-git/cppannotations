#include "main.ih"

Fibo fiboCoro()
{
    size_t returnFibo = 0;
    size_t next = 1;

    while (true)
    {
        size_t ret = returnFibo;

        returnFibo = next;
        next += ret;

        co_yield ret;
    }
}
