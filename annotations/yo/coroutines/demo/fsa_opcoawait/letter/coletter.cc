#include "letter.ih"

Letter coLetter()
{
    char ch;
    while (cin.get(ch))
    {
        if (isalpha(ch))
            cout << "at `" << ch << "' remain in letter\n";
        else if (isdigit(ch))
            co_await Awaitable{ g_digit, "Letter", ch };
        else
            co_await Awaitable{ g_start, "Letter", static_cast<int>(ch) };
    }
    co_await Awaitable{ g_done, "Letter" };
}
