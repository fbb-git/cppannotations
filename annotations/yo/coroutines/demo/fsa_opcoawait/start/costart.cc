#include "start.ih"

Start coStart()
{
    char ch;
    while (cin.get(ch))
    {
        if (isalpha(ch))
            co_await Awaitable{ g_letter, "Start", ch };
        else if (isdigit(ch))
            co_await Awaitable{ g_digit, "Start", ch };
        else
            cout << "at char #" << static_cast<int>(ch) <<
                    ": remain in start\n";
    }
    co_await Awaitable{ g_done, "Start" };
}
//=
