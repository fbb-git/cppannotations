The previous examples predominantly used while-statements to obtain the values
returned by coroutines, many generic algorithms (as well as range-based
for-loops) depend on the availability of tt(begin) and tt(end) members
returning iterators. 

Coroutines (or actually, their handling classes) may also define tt(begin) and
tt(end) members returning iterators. In practice those iterators are input
iterators (cf. section ref(ITERATORS)), providing access to the values
co_yielded by their coroutines. Section ref(ITERATORCONS) specifies their
requirements. For plain types (like tt(size_t) which is co_yielded by
tt(Fibo::next)) iterators should provide the following members:
    itemization(
    it() a prefix increment operator (tt(Iterator &operator++()));
    it() a dereference operator (tt(Type &operator*()));
    it() comparison operators (tt(bool operator==(Iterator const &other))) 
        (and maybe tt(operator!=) returning its complement).
    )

The tt(Iterator) class is a value class. However, except for copy- and
move-constructions, tt(Iterator) objects can only be constructed by
tt(Recursive's begin) and tt(end) members. It has a private constructor
and declares tt(Recursive) as its friend:
    verbinsert(//iter demo/coroiter/recursive/recursive.h)

tt(Iterator's) constructor receives tt(Recursive::d_handle), so it can use 
its own tt(d_handle) to control tt(recursiveCoro's) behavior:
    verbinsert(-s4 //cons demo/coroiter/recursive/iterator.cc)

The member tt(Recursive::begin) ensures that tt(Iterator::operator*) can
immediately provide the next available value by resuming the coroutine. If
that succeeds it passes tt(d_handle) to tt(Iterator's) constructor. If
there are no values it returns 0, which is the tt(Iterator) that's also
returned by tt(Recursive::end):
    verbinsert(-s4 //+beginend demo/coroiter/recursive/recursive.cc)

The dereference operator simply calls and returns the value returned by
tt(State::value()) and the prefix increment operator resumes the coroutine. If
no value was produced it assigns 0 to its tt(d_handle), resulting in tt(true)
when compared to the iterator returned by tt(Recursive::end):
    verbinsert(-s4 //iter demo/coroiter/recursive/iterator.cc)


