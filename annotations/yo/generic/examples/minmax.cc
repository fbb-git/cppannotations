    #include <algorithm>
    #include <iostream>
    #include <functional>

    using namespace std;

    int main()
    {
        vector<size_t> uv(10);

        auto values = minmax(5, 2, less<int>{});
        cout << values.first << " is smaler than " << values.second << '\n';
    }
    // Displays:   2 is smaler than 5
