sect(The Generic Algorithms)
includefile(generic/intro)

    lsubsect(EXEC)(Execution policies)
    includefile(generic/execpol)
    
    lsubsect(ACCU)(accumulate)
    includefile(generic/accumulate)

    lsubsect(ADJDIFF)(adjacent_difference)
    includefile(generic/adjacentdifference)

    lsubsect(ADJFIND)(adjacent_find)
    includefile(generic/adjacentfind)

    lsubsect(XOF)(all_of / any_of / none_of)
    includefile(generic/allof)

    lsubsect(BEGEND)(begin / end)     
    includefile(generic/beginend)

    lsubsect(BINSRCH)(binary_search)
    includefile(generic/binarysearch)

    lsubsect(COPY)(copy / copy_if)
    includefile(generic/copy)

    lsubsect(COPYBACK)(copy_backward)
    includefile(generic/copybackward)

    lsubsect(COUNT)(count / count_if)
    includefile(generic/count)

    lsubsect(EQUAL)(equal)
    includefile(generic/equal)

    lsubsect(EQUALRANGE)(equal_range)
    includefile(generic/equalrange)

    lsubsect(EXCHANGE)(exchange)
    includefile(generic/exchange)

    lsubsect(FILL)(fill / fill_n)
    includefile(generic/fill)

    lsubsect(FIND)(find / find_if / find_if_not)
    includefile(generic/find)

    lsubsect(FINDEND)(find_end)
    includefile(generic/findend)

    lsubsect(FINDFIRST)(find_first_of)
    includefile(generic/findfirstof)

    lsubsect(FOREACH)(for_each)
    includefile(generic/foreach)

    lsubsect(GEN)(generate / generate_n)
    includefile(generic/generate)

    lsubsect(INCLUDES)(includes)
    includefile(generic/includes)

    lsubsect(INNERPROD)(inner_product)
    includefile(generic/innerproduct)

    lsubsect(INMERGE)(inplace_merge)
    includefile(generic/inplacemerge)

    lsubsect(IOTA)(iota)
    includefile(generic/iota)

    lsubsect(ISPART)(is_partitioned)
    includefile(generic/ispartitioned)

    lsubsect(ISPERM)(is_permutation)
    includefile(generic/ispermutation)

    lsubsect(ISSORTED)(is_sorted) 
    includefile(generic/issorted)

    lsubsect(ISSORTEDUNT)(is_sorted_until)
    includefile(generic/issorteduntil)

    lsubsect(ITERSWAP)(iter_swap)  
    includefile(generic/iterswap)

    lsubsect(LEXCOMP)(lexicographical_compare)
    includefile(generic/lexicographicalcompare)

    lsubsect(LOWERBOUND)(lower_bound)
    includefile(generic/lowerbound)

    lsubsect(MAX)(max / min)   
    includefile(generic/max)

    lsubsect(MAXEL)(max_element / min_element / minmax_element)
    includefile(generic/maxelement)

    lsubsect(MERGE)(merge)
    includefile(generic/merge)

    lsubsect(MINMAX)(minmax) 
    includefile(generic/minmax)

    lsubsect(MISMATCH)(mismatch)
    includefile(generic/mismatch)

    lsubsect(MOVEFWD)(move / move_backward)
    includefile(generic/move)

    lsubsect(NEXTPERM)(next_permutation / prev_permutation)
    includefile(generic/nextpermutation)

    lsubsect(NTHEL)(nth_element)
    includefile(generic/nthelement)

    lsubsect(PARTSORT)(partial_sort / partial_sort_copy)
    includefile(generic/partialsort)

    lsubsect(PARTSUM)(partial_sum) 
    includefile(generic/partialsum)

    lsubsect(PARTIT)(partition / partition_point / stable_partition)
    includefile(generic/partition)

    lsubsect(PARTCP)(partition_copy)
    includefile(generic/partitioncopy)

    lsubsect(REDUCE)(reduce)
    includefile(generic/reduce)

    lsubsect(REMOVE)(remove / remove_if / remove_copy / remove_copy_if)
    includefile(generic/remove)        

    lsubsect(REPLACE)(replace / replace_if / replace_copy / replace_copy_if)
    includefile(generic/replace)

    lsubsect(REVERSE)(reverse / reverse_copy)
    includefile(generic/reverse)

    lsubsect(ROTATE)(rotate / rotate_copy)                
    includefile(generic/rotate)

    lsubsect(SAMPLE)(sample)
    includefile(generic/sample)

    lsubsect(SEARCH)(search / search_n)
    includefile(generic/search)

    lsubsect(SETDIF)(set_difference)
    includefile(generic/setdifference)

    lsubsect(SETINT)(set_intersection)
    includefile(generic/setintersection)

    lsubsect(SETSYM)(set_symmetric_difference)
    includefile(generic/setsymmetricdifference)

    lsubsect(SETUNI)(set_union)
    includefile(generic/setunion)

    lsubsect(SORT)(sort / stable_sort )
    includefile(generic/sort)

    lsubsect(SWAP)(swap / swap_ranges)
    includefile(generic/swap)

    lsubsect(TRANSFORM)(transform)
    includefile(generic/transform)

    lsubsect(TRANSRED)(transform_reduce)
    includefile(generic/transformreduce)

    lsubsect(UNINIT)(handling uninitialized memory)
    includefile(generic/uninitialized)
    
    lsubsect(UNIQUE)(unique)
    includefile(generic/unique)

    lsubsect(UNIQUECP)(unique_copy)
    includefile(generic/uniquecopy)

    lsubsect(UPPERBOUND)(upper_bound)
    includefile(generic/upperbound)

    subsect(Heap algorithms)
    includefile(generic/heap)

        lsubsubsect(MAKEHEAP)(The `make_heap' function)
        includefile(generic/makeheap)

        lsubsubsect(POPHEAP)(The `pop_heap' function)
        includefile(generic/popheap)

        lsubsubsect(PUSHHEAP)(The `push_heap' function)
        includefile(generic/pushheap)

        lsubsubsect(SORTHEAP)(The `sort_heap' function)
        includefile(generic/sortheap)

        lsubsubsect(HEAPDEMO)(An example using the heap functions)
        includefile(generic/heapdemo)

COMMENT(
                does: x < lo ? lo : x < hi ? hi : x
                trivial, not very useful?
    lsubsect(CLAMP)(clamp)
    includefile(generic/clamp)

    Merged into other pages:

    lsubsect(COPYIF)(copy_if)
    includefile(generic/copy_if)

    lsubsect(COUNTIF)(count_if)
    includefile(generic/countif)

    lsubsect(FILLN)(fill_n)
    includefile(generic/filln)

    lsubsect(FINDIF)(find_if / find_if_not)
    includefile(generic/findif)

    lsubsect(GENN)(generate_n)
    includefile(generic/generaten)

    lsubsect(MIN)(min)
    includefile(generic/min)

    lsubsect(REMOVEIF)(remove_if)
    includefile(generic/removeif)

    lsubsect(REPLACEIF)(replace_if)
    includefile(generic/replaceif)

    lsubsect(SEARCHN)(search_n)
    includefile(generic/searchn)

    lsubsect(MINEL)(min_element)
    includefile(generic/minelement)

    lsubsect(PARTSORTCP)(partial_sort_copy)
    includefile(generic/partialsortcopy)

    lsubsect(PREVPERM)(prev_permutation)
    includefile(generic/prevpermutation)

    lsubsect(REMOVECP)(remove_copy)
    includefile(generic/removecopy)

    lsubsect(REMOVECPIF)(remove_copy_if)
    includefile(generic/removecopyif)

END)








