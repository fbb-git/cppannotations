#include "union.ih"

void Union::copy(Field type, Union const &other, Field next)
{
    Union tmp{ other, next };    // create a temp. copy
    swap(type, tmp, next);      // swap the current and tmp

    tmp.destroy(type);          // destroy the tmp object (now a 'type' obj.)
}
